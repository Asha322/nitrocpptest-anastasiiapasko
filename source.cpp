#include "json.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "Rectangle.cpp"

using json = nlohmann::json;
using namespace std;

json readJson(string);
bool isSubset(vector<int>, vector<int>);
bool isEqual(vector<int>, vector<int>);
void printParents(Rectangle);

int main(int argc, char* argv[]) {
  json rectangles_data = readJson(argv[1]);
  vector<Rectangle> rectangles;
  vector<Rectangle> intersections;

  // fill the vectors with instances of structure
  for (int i = 0; i < rectangles_data["rects"].size(); i++) {
    Rectangle rect(
        rectangles_data["rects"][i]["x"], rectangles_data["rects"][i]["y"],
        rectangles_data["rects"][i]["w"], rectangles_data["rects"][i]["h"]);
    vector<int> par;
    par.push_back(i);
    rect.setParents(par);
    rectangles.push_back(rect);
  }

  // 2 rectangles intersection
  int originalRects = rectangles.size();
  for (int i = 0; i < originalRects; i++) {
    for (int j = i + 1; j < rectangles.size(); j++) {
      // ensure I am not comparing the same rectangles and do not get stuck in
      // endless loop
      if (isSubset(rectangles.at(j).getParents(),
                   rectangles.at(i).getParents()))
        continue;

      // calculate values of overlap
      if (rectangles.at(i).ifOverlaps(rectangles.at(j))) {
        Rectangle result = rectangles.at(i).calculateOverlap(rectangles.at(j));
        vector<int> par;
        // remember "parents" of the overlap
        par.insert(par.end(), rectangles.at(i).getParents().begin(),
                   rectangles.at(i).getParents().end());
        par.insert(par.end(), rectangles.at(j).getParents().begin(),
                   rectangles.at(j).getParents().end());
        sort(par.begin(), par.end());
        result.setParents(par);
        rectangles.push_back(result);
        intersections.push_back(result);
      }
    }
  }

  // remove duplicates
  for (int i = 0; i < intersections.size(); i++)
    for (int j = 0; j < intersections.size(); j++) {
      if (i == j)
        continue;
      if (isEqual(intersections.at(i).getParents(),
                  intersections.at(j).getParents()))
        intersections.erase(intersections.begin() + j);
    }

  // print result
  for (int i = 0; i < originalRects; i++) {
    cout << i + 1 << ": Rectangle at (" << rectangles.at(i).getX() << ", "
         << rectangles.at(i).getY() << "), w=" << rectangles.at(i).getW()
         << ", h=" << rectangles.at(i).getH() << "." << endl;
  }
  cout << endl << endl;
  for (int i = 0; i < intersections.size(); i++) {
    cout << "Between rectangles ";
    printParents(intersections.at(i));
    cout << "at (" << intersections.at(i).getX() << ","
         << intersections.at(i).getY() << "), w=" << intersections.at(i).getW()
         << ", h=" << intersections.at(i).getH() << "." << endl;
  }
  return 0;
}

json readJson(string s) {
  json j;
  try {
    ifstream i(s);
    if (!(i))
      throw runtime_error("Could not open file");
    i >> j;
    return j;
  } catch (int e) {
    cout << "File not found" << endl;
  }
  return j;
}

bool isSubset(vector<int> v1, vector<int> v2) {
  sort(v1.begin(), v1.end());
  sort(v2.begin(), v2.end());
  return includes(v1.begin(), v1.end(), v2.begin(), v2.end());
}

bool isEqual(vector<int> v1, vector<int> v2) {
  sort(v1.begin(), v1.end());
  sort(v2.begin(), v2.end());
  return (v1 == v2);
}

void printParents(Rectangle r) {
  for (int i = 0; i < r.getParents().size() - 2; i++)
    cout << r.getParents().at(i) + 1 << ", ";
  cout << r.getParents().at(r.getParents().size() - 2) + 1 << " and "
       << r.getParents().at(r.getParents().size() - 1) + 1 << " ";
}
