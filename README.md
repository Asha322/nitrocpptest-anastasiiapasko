# NitroCppTest-AnastasiiaPasko
C++ test for Nitro


Code uses C++ 11 standards, so C++11 should be enabled.

To compile use, for example:

`g++ -std=c++11 source.cpp -o rectangles` 

No other actions or downloads required.

Name of the file with input data is taken as a parameter when the program is started, e.g.:

`./rectangles Data.json`

Supported compilers:
GCC 4.9 - 7.1 (and possibly later), 
Clang 3.4 - 5.0 (and possibly later), 
Microsoft Visual C++ 2015 / Build Tools 14.0.25123.0 (and possibly later), 
Microsoft Visual C++ 2017 / Build Tools 15.1.548.43366 (and possibly later).

To test the code I used the .json files added to the repository.
The program is able to process 10 rectangles of different sizes, but struggles with 10 identical rectangles, especially after replacing structure with a class.

JSON parser taken from here:
https://github.com/nlohmann/json