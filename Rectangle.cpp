#include <vector>
using namespace std;

class Rectangle {
 protected:
  int x;
  int y;
  int w;
  int h;
  vector<int> parents;

 public:
  Rectangle(int X, int Y, int W, int H);
  void setParents(vector<int> parents);
  bool ifOverlaps(Rectangle r);
  Rectangle calculateOverlap(Rectangle r);

  int getX();
  int getY();
  int getW();
  int getH();
  vector<int>& getParents();
};

Rectangle::Rectangle(int X, int Y, int W, int H) {
  x = X;
  y = Y;
  w = W;
  h = H;
}

int Rectangle::getX() {
  return x;
}

int Rectangle::getY() {
  return y;
}

int Rectangle::getW() {
  return w;
}

int Rectangle::getH() {
  return h;
}

vector<int>& Rectangle::getParents() {
  return parents;
}

void Rectangle::setParents(vector<int> V) {
  parents = V;
}

bool Rectangle::ifOverlaps(Rectangle r) {
  if (x > (r.getX() + r.getW()) || r.getX() > (x + w))
    return false;
  if (y > (r.getY() + r.getH()) || r.getY() > (y + h))
    return false;
  return true;
}

Rectangle Rectangle::calculateOverlap(Rectangle r) {
  int l = max(x, r.getX());
  int ri = min(x + w, r.getX() + r.getW());
  int t = max(y, r.getY());
  int b = min(y + h, r.getY() + r.getH());

  return Rectangle(l, t, ri - l, b - t);
}